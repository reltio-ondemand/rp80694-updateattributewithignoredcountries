#!/bin/bash
echo "starting update attributes"
until java -jar ./updateAttribute-1.0-SNAPSHOT-jar-with-dependencies.jar settings.properties
do
  echo "restarting...." >&2
  sleep 1
done
echo "complete!"
