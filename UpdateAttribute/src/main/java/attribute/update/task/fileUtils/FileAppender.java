package attribute.update.task.fileUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.function.Consumer;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

public class FileAppender implements Consumer<Collection<String>> {

    String filePath;
    private BufferedWriter bufferedWriter;
    boolean successfullyInit;

    public FileAppender(String filePath) {
        this(filePath, false);
    }

    public FileAppender(String filePath, boolean append) {
        this.filePath = filePath;
        this.successfullyInit = false;
        init(append);
    }

    private void init(boolean append) {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(new File(filePath), append), 512);
            successfullyInit = true;
        } catch (IOException e) {
            e.printStackTrace();
            successfullyInit = false;
        }
    }

    @Override
    public void accept(Collection<String> data) {
        if (successfullyInit) {
            try {
                for (String item : data) {
                    bufferedWriter.write(item + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void finalHook() {
        if (bufferedWriter != null) {
            try {
                bufferedWriter.flush();
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
