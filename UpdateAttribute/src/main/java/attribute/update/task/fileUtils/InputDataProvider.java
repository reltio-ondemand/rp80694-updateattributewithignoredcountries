package attribute.update.task.fileUtils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;

public class InputDataProvider {

    private String inputFilePath;
    private LineIterator lineIterator;

    public InputDataProvider(String inpitFilePath) {
        this.inputFilePath = inpitFilePath;
    }

    public Iterator<String> getStringIterator() {
        File file = new File(inputFilePath);
        try {
            lineIterator = FileUtils.lineIterator(file, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyIterator();
        }
        return lineIterator;
    }

    public void close() throws IOException {
        if (lineIterator != null) {
            lineIterator.close();
        }
    }
}
