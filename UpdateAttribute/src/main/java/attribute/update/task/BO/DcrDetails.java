package attribute.update.task.BO;

public class DcrDetails {
    //ChangeRequestID	EntityID	AttributeID	ACTION	ATTRIBUTE	EntityID	STATUS
    private String changeRequestID;
    private String entityId;
    private String attribute;//remove
    private String action;
    private String status;
    private String entityType;

    public DcrDetails() {

    }

    public DcrDetails(String changeRequestID, String entityId, String attributeId, String action, String status,
                      String entityType) {
        this.changeRequestID = changeRequestID;
        this.entityId = entityId;
        this.attribute = attributeId;
        this.action = action;
        this.status = status;
        this.entityType = entityType;
    }

    public String getChangeRequestID() {
        return changeRequestID;
    }

    public void setChangeRequestID(String changeRequestID) {
        this.changeRequestID = changeRequestID;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEntityType() {
        return entityType;
    }

    @Override
    public String toString() {
        //ChangeRequestID	EntityID	AttributeID	ACTION	ATTRIBUTE	EntityID	STATUS
        StringBuilder sb = new StringBuilder();
        sb.append(changeRequestID).append(",")
                .append(entityId).append(",")
                .append(attribute).append(",")
                .append(action).append(",")
                .append(status).append(",")
                .append(entityType).append(",");
        return sb.toString();
    }
}
