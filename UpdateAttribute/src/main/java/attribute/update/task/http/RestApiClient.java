package attribute.update.task.http;

import attribute.update.task.BO.DcrDetails;
import attribute.update.task.excluderules.AttributesRulesResolver;
import attribute.update.task.excluderules.EntitiesRulesResolver;
import attribute.update.task.excluderules.ExcludeByDcrDetails;
import attribute.update.task.remediation.RemediateObject;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;


public class RestApiClient {

    protected static final org.apache.log4j.Logger logger = LogManager.getLogger(RestApiClient.class);

    private final String serverUrl;
    private final String tenant;
    private final AuthClient authClient;

    public RestApiClient(String serverUrl, String tenant, AuthClient authClient) {
        this.serverUrl = serverUrl;
        this.tenant = tenant;
        this.authClient = authClient;
    }

    /**
     * this method fetches entity and for every Specialities attribute wuth ignored=true
     * sends request to unignore attribute
     *
     * @param: id of entity
     * @return: Pair of string collections, where left - successful log entries, right - failed
     */


    public Pair<Collection<String>, Collection<String>> getDCRAndUpdateAttributes(String entityId) {
        Collection<String> successfulRequests = new ArrayList<>(), failedRequests = new ArrayList<>();
        //List<RemediateObject> attributesToUpdate = new ArrayList<>();
        try {
            JSONArray dcrs = getDCR(entityId);
            if (dcrs == null) failedRequests.add(entityId);
            else {
                //String dcrURi = entity.getString("uri").substring("changeRequests/".length());
                List<DcrDetails> dcrDetailsList = parseDCR(dcrs);
                for (DcrDetails dcr : dcrDetailsList) {
                    if (ExcludeByDcrDetails.isExcludeFromDCR(dcr.getStatus(), dcr.getAction(),
                            new ArrayList<RemediateObject>(), dcr.getAttribute())) {
                        failedRequests.add(dcr.toString());
                    } else {

                        successfulRequests.add(dcr.toString());
                    }
                    //System.out.println(dcr);
                }

            }
            /*  if (EntitiesRulesResolver.entityExcludedByType(entityType)) {
                failedRequests.add(toExcludeRow(entityId, "", "", "", Boolean.FALSE.toString(), "non-acceptable type: " + entityType));
            } else {
                boolean hasCountry = false , hasReltioCW  = false;
                String countryCode = "";
                JSONArray countries = attributes.getJSONArray("Country");
                if (countries != null) {
                    for (int i = 0; i <countries.length() ; i++) {
                        JSONObject jsonObject = countries.getJSONObject(i);
                        countryCode = jsonObject.getString("lookupCode");
                        if(jsonObject.getBoolean("ov")){
                            hasCountry = true;
                            break;
                        }
                    }
                }

                JSONArray crosswalks =  entity.getJSONArray("crosswalks");
                for (int i = 0; i < crosswalks.length(); i++) {
                    JSONObject crosswalk  = crosswalks.getJSONObject(i);
                    String type = crosswalk.getString("type");
                    if(type.equals("configuration/sources/Reltio")){
                        hasReltioCW = true;
                        break;
                    }
                }

                if (hasReltioCW) {
                    // exclude entities with Reltio crosswalk
                    failedRequests.add(toExcludeRow(entityId, hasCountry, hasReltioCW, countryCode, "has reltio crosswalk"));

                } else {
                    String finalCountryCode = countryCode;
                    attributes.keys().forEachRemaining(key -> processAttributes(entity, finalCountryCode, key, attributes.getJSONArray(key),
                            attributesToUpdate, failedRequests, entityType));

                    if (!attributesToUpdate.isEmpty()) {
                        successfulRequests.add(toRemediateRow(entityId, winnerUri, attributesToUpdate, hasCountry, hasReltioCW, countryCode));
                    } else {
                        failedRequests.add(toExcludeRow(entityId, hasCountry, hasReltioCW, countryCode, "no compliant attributes"));
                    }
                }
            }*/
            return new ImmutablePair<>(successfulRequests, failedRequests);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public Pair<Collection<String>, Collection<String>> getEntityAndUpdateAttributes(String entityId) {
        Collection<String> successfulRequests = new ArrayList<>(), failedRequests = new ArrayList<>();
        List<RemediateObject> attributesToUpdate = new ArrayList<>();
        try {
            JSONObject entity = getEntity(entityId);
            String winnerUri = entity.getString("uri").substring("entities/".length());
            JSONObject attributes = (JSONObject) entity.get("attributes");
            String entityType = entity.getString("type");

            if (EntitiesRulesResolver.entityExcludedByType(entityType)) {
                failedRequests.add(toExcludeRow(entityId, "", "", "", Boolean.FALSE.toString(), "non-acceptable type: " + entityType));
            } else {
                boolean hasCountry = false, hasReltioCW = false;
                String countryCode = "";
                JSONArray countries = attributes.getJSONArray("Country");
                if (countries != null) {
                    for (int i = 0; i < countries.length(); i++) {
                        JSONObject jsonObject = countries.getJSONObject(i);
                        countryCode = jsonObject.getString("lookupCode");
                        if (jsonObject.getBoolean("ov")) {
                            hasCountry = true;
                            break;
                        }
                    }
                }

                JSONArray crosswalks = entity.getJSONArray("crosswalks");
                for (int i = 0; i < crosswalks.length(); i++) {
                    JSONObject crosswalk = crosswalks.getJSONObject(i);
                    String type = crosswalk.getString("type");
                    if (type.equals("configuration/sources/Reltio")) {
                        hasReltioCW = true;
                        break;
                    }
                }

                /*if (hasReltioCW) {
                    // exclude entities with Reltio crosswalk
                    failedRequests.add(toExcludeRow(entityId, hasCountry, hasReltioCW, countryCode, "has reltio crosswalk"));

                }*/ //else {
                String finalCountryCode = countryCode;
                attributes.keys().forEachRemaining(key -> processAttributes(entity, finalCountryCode, key, attributes.getJSONArray(key),
                        attributesToUpdate, failedRequests, entityType));

                if (!attributesToUpdate.isEmpty()) {
                    List<DcrDetails> dcrs = parseDCR(getDCR(entityId));
                    for (DcrDetails dcr : dcrs) {
                        if (ExcludeByDcrDetails.isExcludeFromDCR(dcr.getStatus(), dcr.getAction(),
                                attributesToUpdate, dcr.getAttribute())) {
                            System.out.println("Rule2 Applied "+dcr.getAttribute());
                            failedRequests.add(toExcludeRow(entityId, hasCountry, hasReltioCW,
                                    countryCode, "DCR Rule2 ->" + dcr.getAttribute()));
                        }
                    }
                    if (attributesToUpdate.isEmpty()) {

                        failedRequests.add(toExcludeRow(entityId, hasCountry, hasReltioCW, countryCode, "DCR ALL are" +
                                " dummy" + dcrs.get(0).getChangeRequestID()));
                    } else {
                        successfulRequests.add(toRemediateRow(entityId, winnerUri, attributesToUpdate, hasCountry, hasReltioCW, countryCode));
                    }
                }
                else {
                    failedRequests.add(toExcludeRow(entityId, hasCountry, hasReltioCW, countryCode, "no compliant attributes"));
                }
            }

            return new ImmutablePair<>(successfulRequests, failedRequests);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    public void unignoreAttributesAndDeleteHubCrosswalks(List<RemediateObject> attributesToUpdate, boolean doRemediation) throws IOException {
        List<String> deletedCrosswalks = new ArrayList<>();
        for (RemediateObject remediateObject : attributesToUpdate) {
            String crosswalkUri = remediateObject.getHubCallbackCrosswalkUri();
            if (crosswalkUri != null && deletedCrosswalks.contains(crosswalkUri)) {
                remediateObject.clearHubCallbackCrosswalkUri();
                crosswalkUri = null;
            }
            if (attributeUpdate(remediateObject, doRemediation) && crosswalkUri != null) {
                deletedCrosswalks.add(crosswalkUri);
            }
        }
    }

    private static String toExcludeRow(String entityId, boolean hasCountry, boolean hasReltioCW, String countryCode, String reason) {
        return toExcludeRow(entityId, Boolean.toString(hasCountry), Boolean.toString(hasReltioCW), countryCode, Boolean.FALSE.toString(), reason);
    }

    public static String toExcludeRow(String entityId, String hasCountry, String hasReltioCW, String countryCode, String dcr, String reason) {
        return entityId + "," + hasCountry + "," + countryCode + "," + hasReltioCW + "," + dcr + "," + reason;
    }

    private static String toRemediateRow(String entityId, String winnerUri, Collection<RemediateObject> attributesToUpdate, boolean hasCountry, boolean hasReltioCW, String countryCode) {
        Collection<String> attrUris = new ArrayList<>();
        Collection<String> hubCrosswalks = new HashSet<>();
        attributesToUpdate.forEach(r -> {
            attrUris.add(r.getAttributeUri());
            String hubCallbackCrosswalkUri = r.getHubCallbackCrosswalkUri();
            if (hubCallbackCrosswalkUri != null) {
                hubCrosswalks.add(hubCallbackCrosswalkUri);
            }
        });
        String attributes = String.join(",", attrUris);
        String crosswalks = String.join(",", hubCrosswalks);
        attributes += crosswalks.isEmpty() ? "" : "," + crosswalks;
        return toRemediateRow(entityId, winnerUri, attributes, Boolean.toString(hasCountry), Boolean.toString(hasReltioCW), countryCode);
    }

    public static String toRemediateRow(String entityId, String winnerUri, String attributes, String hasCountry, String hasReltioCW, String countryCode) {
        return String.format("%s, %s, %s, %s, %s, %s, ",
                entityId, winnerUri, hasCountry, countryCode, hasReltioCW, attributes);
    }

    public static String toRemediateRow(String changeID, String entiyId, String id, String attribute,
                                        String action, String eId, String status, String type) {
        return String.format("%s, %s, %s, %s, %s, %s, %s, %s",
                changeID, entiyId, id, attribute, action, eId, status, type);
    }
//"changeRequestID", "EntityId", "Id", "Attribute", "Action", "entiyID", "Status", "Type"

    public Pair<Collection<String>, Collection<String>> getEntityForCriteria(String entityId) {
        logger.info("entityId=" + entityId);
        HttpGet getEntity = new HttpGet(String.format("%s/api/%s/entities/%s",
                serverUrl,
                tenant,
                entityId));
        getEntity.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
        getEntity.setHeader("Content-Type", "application/json");

        String responseContent = null;
        Collection<String> successfulRequests = new ArrayList<>(), failedRequests = new ArrayList<>();

        CloseableHttpResponse response = null;
        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            response = httpClient.execute(getEntity);
            responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());

            JSONObject main = new JSONObject(responseContent);
            //JSONObject main = (JSONObject) JSONValue.parse(responseContent);
            JSONObject attributes = (JSONObject) main.get("attributes");
            attributes.has("Country");
            boolean hasCountry = false, hasReltioCW = false;
            String country = "", reltioCW = "";
            JSONArray countries = attributes.getJSONArray("Country");
            for (int i = 0; i < countries.length(); i++) {

                JSONObject jsonObject = countries.getJSONObject(i);
                if (jsonObject.getBoolean("ov")) {
                    hasCountry = true;
                    country = jsonObject.getString("uri");
                    break;
                }
            }

            JSONArray crosswalks = main.getJSONArray("crosswalks");
            for (int i = 0; i < crosswalks.length(); i++) {
                JSONObject crosswalk = crosswalks.getJSONObject(i);
                String type = crosswalk.getString("type");
                if (type.equals("configuration/sources/Reltio")) {
                    hasReltioCW = true;
                    reltioCW = crosswalk.getString("uri");
                    break;
                }
            }

            if (hasCountry || hasReltioCW) {
                logger.info(String.format("uri=%s, country=%s , reltioCW=%s", main.getString("uri"), country,
                        reltioCW));
                successfulRequests.add(main.getString("uri"));
            } else {
                failedRequests.add(main.getString("uri"));
                logger.info(String.format("uri=%s", main.getString("uri")));

            }


            return new ImmutablePair<>(successfulRequests, failedRequests);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }


    public boolean attributeUpdate(RemediateObject remediateObject) {
        logger.info("uri for update=" + remediateObject.getAttributeUri());
        return true;

    }

    public boolean attributeUpdate(RemediateObject remediateObject, boolean process) throws IOException {
        return !process || unignoreAttribute(remediateObject.getAttributeUri()) && deleteCrosswalk(remediateObject.getHubCallbackCrosswalkUri());
    }

    public boolean deleteCrosswalk(String crosswalkUri) {
        if (crosswalkUri != null) {
            String requestUri = String.format("%s/api/%s/%s",
                    serverUrl,
                    tenant,
                    crosswalkUri);

            try {
                logger.info("Sending request to " + requestUri);
                HttpDelete deleteRequest = new HttpDelete(requestUri);

                deleteRequest.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
                deleteRequest.setHeader("Content-Type", "application/json");
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("value", false);

                CloseableHttpClient httpClient = HttpClients.createDefault();
                CloseableHttpResponse response = httpClient.execute(deleteRequest);
                if (response == null || response.getStatusLine().getStatusCode() >= 400) {
                    String errorDetails;
                    try {
                        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
                        JSONObject responseJson = new JSONObject(responseContent);
                        errorDetails = responseJson.getString("errorDetailMessage");
                        errorDetails = errorDetails != null ? errorDetails : "";
                    } catch (Exception ignore) {
                        errorDetails = "";
                    }
                    logger.error("Failed to delete crosswalk " + requestUri + ", details: " + errorDetails);
                } else {
                    return true;
                }
            } catch (Exception e) {
                logger.error("Failed to send unIgnore request to " + requestUri);
                e.printStackTrace();
            }
        }
        return false;
    }

    public boolean unignoreAttribute(String uri) throws IOException {
        if (uri != null && uri.equals("dummy")) {
            return true;
        }
        String requestUri = String.format("%s/api/%s/%s/ignore",
                serverUrl,
                tenant,
                uri);

        logger.info("Sending request to " + requestUri);
        HttpPut postAttribute = new HttpPut(requestUri);

        postAttribute.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
        postAttribute.setHeader("Content-Type", "application/json");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", false);

        postAttribute.setEntity(new StringEntity(jsonObject.toString()));

        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(postAttribute);
        if (response == null || response.getStatusLine().getStatusCode() != 202) {
            logger.error("Failed to send unIgnore request to " + requestUri);
        } else {
            return true;
        }
        return false;
    }

    public void processAttributes(JSONObject entity, String countryCode, String attributeName, JSONArray attributeValues, Collection<RemediateObject> toRemidiate, Collection failures, String entityType) throws JSONException {
        for (int i = 0; i < attributeValues.length(); i++) {
            JSONObject attribute = ((JSONArray) attributeValues).getJSONObject(i);
            String uri = attribute.getString("uri");
            if (!AttributesRulesResolver.attributeExcluded(countryCode, attributeName, attribute, entityType)) {
                boolean ignore = attribute.has("ignored") ? attribute.getBoolean("ignored") : Boolean.FALSE;
                if (ignore) {

                    toRemidiate.add(RemediateObject.toRemediate(uri, Optional.of(entity)));
                }
                Object value = attribute.get("value");
                if (value instanceof JSONObject) {
                    processInnerAttributes(entity, countryCode, attributeName, (JSONObject) value, toRemidiate, failures, entityType);
                }
            } else {
                logger.info("Attribute " + attributeName + " is excluded due rules");
            }
        }
    }

    private void processInnerAttributes(JSONObject entity, String countryCode, String parentAttributeName, JSONObject attribute, Collection<RemediateObject> toRemediate, Collection failures, String entityType) {
        String uri;
        Boolean ignore;
        if (attribute.has("uri")) {
            uri = attribute.getString("uri");
            ignore = attribute.has("ignored") ? attribute.getBoolean("ignored") : Boolean.FALSE;
            if (ignore) {
                toRemediate.add(RemediateObject.toRemediate(uri, Optional.of(entity)));
            }
        } else {
            attribute.keys().forEachRemaining(key -> processAttributes(entity, countryCode, parentAttributeName + "." + key, attribute.getJSONArray(key), toRemediate, failures, entityType));
        }
    }

    public void foundUnFixedAttributed(JSONObject entity, JSONArray attributeValues, List<String> attributesUris, List<RemediateObject> unfixedAttributes) throws JSONException {
        for (int i = 0; i < attributeValues.length(); i++) {
            JSONObject attribute = (attributeValues).getJSONObject(i);
            String uri = attribute.getString("uri");
            if (attributesUris.contains(uri)) {
                boolean ignore = attribute.has("ignored") ? attribute.getBoolean("ignored") : Boolean.FALSE;
                if (ignore) {
                    unfixedAttributes.add(RemediateObject.toRemediate(uri, Optional.of(entity)));
                } else {
                    boolean ov = attribute.has("ov") ? attribute.getBoolean("ov") : Boolean.FALSE;
                    if (!ov && !foundEndDatedCrosswalk(uri, entity)) {
                        unfixedAttributes.add(RemediateObject.toRemediate(uri, Optional.of(entity)));
                    }
                }
            }
            Object value = attribute.get("value");
            if (value instanceof JSONObject) {
                foundUnFixedAttributedInInnerAttributes(entity, attributeValues, attributesUris, unfixedAttributes, (JSONObject) value);
            }
        }
    }

    private static boolean foundEndDatedCrosswalk(String attributeUri, JSONObject entity) {
        boolean found = false;
        JSONArray crosswalks = entity.getJSONArray("crosswalks");
        for (int i = 0; i < crosswalks.length(); i++) {
            JSONObject crosswalk = crosswalks.getJSONObject(i);
            JSONArray attributes = crosswalk.getJSONArray("attributes");
            if (attributes != null) {
                for (int j = 0; j < attributes.length(); j++) {
                    if (attributeUri.equals(attributes.getString(j))) {
                        String deleteDate = crosswalk.getString("deleteDate");
                        if (deleteDate != null) {
                            found = true;
                            break;
                        }
                    }
                }
                if (found) {
                    break;
                }
            }
        }
        return found;
    }

    private void foundUnFixedAttributedInInnerAttributes(JSONObject entity, JSONArray attributeValues, List<String> attributesUris, List<RemediateObject> unfixedAttributes, JSONObject attribute) {
        if (attribute.has("uri")) {
            String uri = attribute.getString("uri");
            if (attributesUris.contains(uri)) {
                boolean ignore = attribute.has("ignored") ? attribute.getBoolean("ignored") : Boolean.FALSE;
                if (ignore) {
                    unfixedAttributes.add(RemediateObject.toRemediate(uri, Optional.of(entity)));
                } else {
                    boolean ov = attribute.has("ov") ? attribute.getBoolean("ov") : Boolean.FALSE;
                    if (!ov && !foundEndDatedCrosswalk(uri, entity)) {
                        unfixedAttributes.add(RemediateObject.toRemediate(uri, Optional.of(entity)));
                    }
                }
            }
        } else {
            attribute.keys().forEachRemaining(key -> foundUnFixedAttributed(entity, attribute.getJSONArray(key), attributesUris, unfixedAttributes));
        }
    }

    private JSONObject getEntity(String entityId) throws Exception {
        logger.info("entityId=" + entityId);
        HttpGet getEntity = new HttpGet(String.format("%s/api/%s/entities/%s",
                serverUrl,
                tenant,
                entityId));
        getEntity.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
        getEntity.setHeader("Content-Type", "application/json");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(getEntity);
        if (response.getStatusLine().getStatusCode() >= 400) {
            throw new Exception(String.format("Entity %s getting error: %s", entityId, response.getStatusLine().getStatusCode()));
        }
        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
        return new JSONObject(responseContent);
    }


    private JSONArray getDCR(String entityId) throws Exception {
        logger.info("entityId=" + entityId);

        URIBuilder builder = new URIBuilder(String.format("%s/api/%s/changeRequests",
                serverUrl,
                tenant
        ));
        builder.setParameter("filter", String.format("equals(objectURI , entities/%s)", entityId));

        HttpGet getEntity = new HttpGet(builder.build());
        getEntity.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
        getEntity.setHeader("Content-Type", "application/json");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(getEntity);
        if (response.getStatusLine().getStatusCode() >= 400) {
            throw new Exception(String.format("Entity %s getting error: %s", entityId, response.getStatusLine().getStatusCode()));
        }
        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
        return new JSONArray(responseContent);
    }

    public List<RemediateObject> getIgnoredOrNonOvAttributes(String entityId, List<RemediateObject> toVerify) throws Exception {
        JSONObject entity = getEntity(entityId);
        JSONObject attributes = (JSONObject) entity.get("attributes");
        List<String> attributesUris = new ArrayList<>();
        for (RemediateObject remediateObject : toVerify) {
            if (!remediateObject.getAttributeUri().equals("dummy"))
                attributesUris.add(remediateObject.getAttributeUri());
        }
        List<RemediateObject> unfixedAttributes = new ArrayList<>();
        attributes.keys().forEachRemaining(key -> foundUnFixedAttributed(entity, attributes.getJSONArray(key), attributesUris, unfixedAttributes));
        return unfixedAttributes;
    }


    private List<DcrDetails> parseDCR(JSONArray jsonObjectArray) {

        List<DcrDetails> dcrDetailsList = new ArrayList<>();
        for (int k = 0; k < jsonObjectArray.length(); k++) {

            JSONObject jsonObject = jsonObjectArray.getJSONObject(k);
            String uri = jsonObject.getString("uri");
            JSONObject change = jsonObject.getJSONObject("changes");
            Iterator<String> keys = change.keys();
            DcrDetails detail = null;

            //sb.append(",");
            while (keys.hasNext()) {

                String key = keys.next();
                // sb.append(",");
                JSONObject externalInfo = jsonObject.getJSONObject("externalInfo");
            /*String entID = externalInfo.getString("IND_ID");
            sb.append(entID);
            sb.append(",");*/
                String state = jsonObject.getString("state");


                //String id = changeDetails.getJSONObject(0).getString("id");
                JSONArray changeDetails = change.getJSONArray(key);
                for (int i = 0; i < changeDetails.length(); i++) {
                    detail = new DcrDetails();
                    detail.setChangeRequestID(uri);
                    detail.setEntityId(key);
                    detail.setStatus(state);
                    detail.setEntityType(externalInfo.has("DCR_Type") ? externalInfo.getString("DCR_Type") : "");

                    String type = changeDetails.getJSONObject(i).has("type") ?
                            changeDetails.getJSONObject(i).getString("type") : "";

                    String path = changeDetails.getJSONObject(i).has("attributePath") ?
                            changeDetails.getJSONObject(i).getString("attributePath") : "";
                    // sb.append(id);
                    detail.setAttribute(path);
                    detail.setAction(type);
                    dcrDetailsList.add(detail);
                    //System.out.println(detail.toString());
                }
                // sb.append(", ");
                //sb.append(path);
                //  sb.append(", ");
                // sb.append(type);
            }
        }
        return dcrDetailsList;
    }
}
