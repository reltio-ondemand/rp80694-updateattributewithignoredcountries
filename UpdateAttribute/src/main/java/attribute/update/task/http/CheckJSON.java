package attribute.update.task.http;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

public class CheckJSON {

    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject(readResource("/Users/prashant.shimpi/Library/Preferences/IntelliJIdea2019.2/scratches/mod.json"));
        JSONObject attributes = jsonObject.getJSONObject("attributes");
        Collection<String> successfulRequests = new ArrayList<>(), failedRequests = new ArrayList<>();
        attributes.keys().forEachRemaining(key -> {
            processAttributes(attributes.getJSONArray(key) ,successfulRequests, failedRequests);
        });
        successfulRequests.forEach(e -> System.out.println(e));
        failedRequests.forEach(e -> System.out.println(e));

    }


    public static String readResource(final String resourcePath) {
        try {
            File file = new File(resourcePath);
            return FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return "";
        }
    }

    public static void processAttributes(JSONArray attributesJSONArray , Collection success , Collection failures) throws JSONException {
        for (int i = 0; i < attributesJSONArray.length(); i++) {
            JSONObject attribute = ((JSONArray) attributesJSONArray).getJSONObject(i);
            String uri = attribute.getString("uri");
            Boolean ignore = attribute.has("ignored") ? attribute.getBoolean("ignored") : Boolean.FALSE;
            if (ignore) {
                if(attributeUpdate(uri)){
                    success.add(uri);
                }
            }
            Object value = attribute.get("value");
            if (value instanceof JSONObject) {
                processInnerAttributes((JSONObject) value, success , failures);
            }
        }
    }

    private static void processInnerAttributes(JSONObject value, Collection success , Collection failures) {
        String uri;
        Boolean ignore;
        if (value.has("uri")) {

            uri = value.getString("uri");
            ignore = value.has("ignored") ? value.getBoolean("ignored") : Boolean.FALSE;
            if (ignore) {
                if(attributeUpdate(uri)){
                    success.add(uri);
                }
            }
        } else {
            value.keys().forEachRemaining(key -> processAttributes(value.getJSONArray(key) , success , failures));
        }
    }


  /*  public static void processAttributes(JSONArray input) throws JSONException {
        for (int i = 0; i < input.length(); i++) {
            if(input.get(i) instanceof JSONObject) {
                JSONObject attribute = input.getJSONObject(i);
                processInnerAttributes(attribute);
            }
           *//* if (attribute.has("uri") && attribute.has("ignored")) {
                String uri = attribute.getString("uri");
                Boolean ignore = attribute.has("ignored") ? attribute.getBoolean("ignored") : Boolean.FALSE;
                if (ignore) {
                    attributeUpdate(uri);
                }
              }
                Object value = attribute.get("value");
                if (value instanceof JSONObject) {
                    processInnerAttributes((JSONObject) value);
                }*//*

        }
    }

    private static  void processInnerAttributes(JSONObject value) {
        String uri;
        Boolean ignore;
        if(value.has("uri") && value.has("ignored")) {
            uri = value.getString("uri");
            ignore =  value.getBoolean("ignored");
            if (ignore) {
                attributeUpdate(uri);
            }
        }else {
            value.keys().forEachRemaining( key -> {
                if(value.get(key)instanceof  JSONArray) {
                    processAttributes(value.getJSONArray(key));
                }else if(value.get(key)instanceof  JSONObject) {
                    processInnerAttributes(value.getJSONObject(key));
                }
            });
        }
    }
*/

    public static boolean attributeUpdate(String uri) {
        System.out.println(uri);
        return true;
    }
}
