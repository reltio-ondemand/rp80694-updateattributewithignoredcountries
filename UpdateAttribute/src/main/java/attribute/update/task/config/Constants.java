package attribute.update.task.config;

public interface Constants {

    String THREAD_COUNT = "threadCount";
    String BATCH_SIZE = "batchSize";
    String FILE_PATH = "filePath";
    String BASE_DELAY = "baseDelay";
    String USER = "user";
    String PASSWORD = "password";
    String SERVER_URL = "serverUrl";
    String AUTH_SERVER = "authServer";
    String TENANT = "tenant";
    String SKIP_STATUS_CHECK = "skipStatusCheck";

    String ERROR_OUTPUT_FILEPATH = "errorOutputFilepath";


}
