package attribute.update.task.config;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ConfigProvider {

    private static Logger logger = LoggerFactory.getLogger(ConfigProvider.class);

    public static Configuration getConfig(String[] args) throws Exception {
        if (args.length != 1) {
            logger.error("Path to file with properties should be specified as execution argument");
            return null;
        }
        return getConfigFromFile(args[0]);
    }


    private static Configuration getConfigFromFile(String propertyFilePath) {
        if (propertyFilePath == null)
            return  null;
        Configuration config;
        File configFile;
        try {
            configFile = new File(propertyFilePath);
            config = new PropertiesConfiguration(configFile);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        CompositeConfiguration configuration = new CompositeConfiguration();
        configuration.addConfiguration(new SystemConfiguration());
        configuration.addConfiguration(config);
        return configuration;
    }

}
