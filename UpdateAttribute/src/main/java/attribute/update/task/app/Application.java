package attribute.update.task.app;

import attribute.update.task.excluderules.AttributesRulesResolver;
import attribute.update.task.excluderules.EntitiesRulesResolver;
import attribute.update.task.fileUtils.FileAppender;
import attribute.update.task.fileUtils.InputDataProvider;
import attribute.update.task.http.AuthClient;
import attribute.update.task.http.RestApiClient;
import attribute.update.task.remediation.RemediateObject;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.LogManager;

import java.io.FileReader;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class Application {

    protected static final org.apache.log4j.Logger logger = LogManager.getLogger(Application.class);

    private static final String THREADS_DEFAULT = "200";
    static Properties prop = new Properties();
    private static final long START_TIME = System.currentTimeMillis();

    private static class BatchWorker implements Runnable {

        private final AtomicLong successfulRequestCount;
        private final AtomicLong failedRequestCount;
        private final AtomicLong requestCount;
        private final Set<String> failedInputData;
        private final FileAppender successWriter;
        private final FileAppender failureWriter;
        private final RestApiClient restApiClient;
        private final Collection<String> processedEntities;
        private final FileAppender processedEntitiesLog;
        private final boolean doRemediation;
        private final FileAppender errorsFileAppender;
        private final boolean doVerification;

        List<String> toProcess;

        public BatchWorker(
                FileAppender errorsFileAppender,
                FileAppender processedEntitiesLog,
                Collection<String> processedEntities,
                RestApiClient restApiClient,
                List<String> toProcess,
                AtomicLong successfullRequestCount,
                AtomicLong failedRequestCount,
                AtomicLong requestCount,
                Set<String> failedInputData,
                FileAppender successWriter,
                FileAppender failureWriter,
                boolean doRemediation, boolean doVerification) {
            this.restApiClient = restApiClient;
            this.toProcess = new ArrayList<>(toProcess);
            this.successfulRequestCount = successfullRequestCount;
            this.failedRequestCount = failedRequestCount;
            this.requestCount = requestCount;
            this.failedInputData = failedInputData;
            this.successWriter = successWriter;
            this.failureWriter = failureWriter;
            this.processedEntities = processedEntities;
            this.processedEntitiesLog = processedEntitiesLog;
            this.doRemediation = doRemediation;
            this.errorsFileAppender = errorsFileAppender;
            this.doVerification = doVerification;
        }


        public void run1(){

            long start = System.currentTimeMillis();
            Iterator<String> stringIterator = toProcess.iterator();
            List<String> successLogEntries = new ArrayList<>(), failureLogEntries = new ArrayList<>();
            while (stringIterator.hasNext()) {
                String inputString = stringIterator.next();
                // entityId is on first position
                String[] parts = inputString.split(",");
                String entityId = parts[0];
                boolean skipped = false;
                if (processedEntities.contains(entityId)) {
                    //skip
                    logger.info("Entity " + entityId + " already processed. Skip");
                    skipped = true;
                } else {

                }
                if (!skipped) {
                    requestCount.incrementAndGet();
                    processedEntitiesLog.accept(Collections.singletonList(entityId));
                }

            }
            successWriter.accept(successLogEntries);
            failureWriter.accept(failureLogEntries);
            long end = System.currentTimeMillis();
            logger.info(MessageFormat.format(
                    "\t\tUpdated {0} objects in {1} sec: throughput is {2}",
                    toProcess.size(),
                    (end - start) / 1000.0,
                    toProcess.size() / ((end - start) / 1000.0)));

            logger.info("Overall progress: " + requestCount + ", throughput " + ((requestCount.get() * 1000.0) / (System.currentTimeMillis() - START_TIME)));
        }

        @Override
        public void run() {
            long start = System.currentTimeMillis();
            Iterator<String> stringIterator = toProcess.iterator();
            List<String> successLogEntries = new ArrayList<>(), failureLogEntries = new ArrayList<>();
            while (stringIterator.hasNext()) {
                String inputString = stringIterator.next();
                // entityId is on first position
                String[] parts = inputString.split(",");
                String entityId = parts[0];
                boolean skipped = false;
                if (processedEntities.contains(entityId)) {
                    //skip
                    logger.info("Entity " + entityId + " already processed. Skip");
                    skipped = true;
                } else if (doRemediation) {
                    try {
                        List<RemediateObject> toRemediate = extractRemediatedObjects(parts);
                        if (toRemediate.size() > 0) {
                            restApiClient.unignoreAttributesAndDeleteHubCrosswalks(toRemediate, true);
                            successfulRequestCount.incrementAndGet();  //if result != null then entity processed
                            successLogEntries.addAll(Collections.singletonList(inputString));
                        }
                    } catch (Exception e) {
                        logger.error("Failed during remediation: " + e.getMessage());
                        String output = inputString + "," + e.getMessage();
                        errorsFileAppender.accept(Collections.singletonList(output));
                    }
                } else if (doVerification) {
                    try {
                        List<RemediateObject> toVerify = extractRemediatedObjects(parts);
                        if (toVerify.size() > 0) {
                            try {
                                List<RemediateObject> ignoredOrNonOvAttributes = restApiClient.getIgnoredOrNonOvAttributes(entityId, toVerify);
                                if (ignoredOrNonOvAttributes.size() > 0) {
                                    successfulRequestCount.incrementAndGet();
                                    successLogEntries.addAll(Collections.singletonList(inputString));
                                } else {
                                    failedRequestCount.incrementAndGet();
                                    failureLogEntries.addAll(Collections.singletonList(inputString));
                                }
                            } catch (Exception e) {
                                logger.error("Failed during remediation: " + e.getMessage());
                                String output = inputString + "," + e.getMessage();
                                errorsFileAppender.accept(Collections.singletonList(output));
                            }
                        }
                    } catch (Exception e) {
                        logger.error("Failed during remediation: " + e.getMessage());
                        String output = inputString + "," + e.getMessage();
                        errorsFileAppender.accept(Collections.singletonList(output));
                    }
                } else if(true) {
                    if (EntitiesRulesResolver.entityExcludedDueDCR(entityId)) {
                        failureLogEntries.add(RestApiClient.toExcludeRow(entityId, "", "", "", Boolean.TRUE.toString(), "DCR found"));
                    } else {
                        Pair<Collection<String>, Collection<String>> response = restApiClient.getEntityAndUpdateAttributes(entityId);
                        if (response != null) {
                            successfulRequestCount.incrementAndGet();  //if result != null then entity processed
                            successLogEntries.addAll(response.getLeft());
                            failureLogEntries.addAll(response.getRight());
                        } else {
                            failedRequestCount.incrementAndGet();
                            failedInputData.add(inputString);
                        }
                    }
                }else {
                    Pair<Collection<String>, Collection<String>> response = restApiClient.getDCRAndUpdateAttributes(entityId);
                    if (response != null) {
                        successfulRequestCount.incrementAndGet();  //if result != null then entity processed
                        successLogEntries.addAll(response.getLeft());
                        failureLogEntries.addAll(response.getRight());
                    } else {
                        failedRequestCount.incrementAndGet();
                        failedInputData.add(inputString);
                    }
                }
                if (!skipped) {
                    requestCount.incrementAndGet();
                    processedEntitiesLog.accept(Collections.singletonList(entityId));
                }
            }
            successWriter.accept(successLogEntries);
            failureWriter.accept(failureLogEntries);
            long end = System.currentTimeMillis();
            logger.info(MessageFormat.format(
                    "\t\tUpdated {0} objects in {1} sec: throughput is {2}",
                    toProcess.size(),
                    (end - start) / 1000.0,
                    toProcess.size() / ((end - start) / 1000.0)));

            logger.info("Overall progress: " + requestCount + ", throughput " + ((requestCount.get() * 1000.0) / (System.currentTimeMillis() - START_TIME)));
        }
    }

    private static List<RemediateObject> extractRemediatedObjects(String[] parts) {
        if (parts == null || parts.length == 0) {
            return Collections.emptyList();
        } else {
            ArrayList<RemediateObject> remediateObjects = new ArrayList<>();
            ArrayList<String> crosswalks = new ArrayList<>();
            for (String part : parts) {
                String trimmed = part.trim();
                if (trimmed.contains("/attributes/")) {
                    remediateObjects.add(RemediateObject.toRemediate(trimmed, Optional.empty()));
                } else if (trimmed.contains("/crosswalks/")) {
                    crosswalks.add(trimmed);
                }
            }
            if (!crosswalks.isEmpty() && crosswalks.size() <= remediateObjects.size()) {
                for (int i = 0; i < crosswalks.size(); i++) {
                    RemediateObject remediateObject = remediateObjects.get(i);
                    remediateObject.setHubCallbackCrosswalkUri(crosswalks.get(i));
                }
            }else if(!crosswalks.isEmpty() && crosswalks.size() > remediateObjects.size()){
                for (int i = 0; i < crosswalks.size(); i++) {
                    RemediateObject remediateObject = new RemediateObject("dummy", crosswalks.get(i));
                    remediateObjects.add(remediateObject);
                }
            }
            return remediateObjects;
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        if (args.length == 0) {
            System.out.println("Input Properties not specified");
            return;
        }
        String file = args[0];

        prop.load(new FileReader(file));

        String processedEntitiesFile = prop.getProperty("PROCESSED_ENTITIES_FILE", "processed_entities.csv");
        String toRemediateFile = prop.getProperty("SUCCESSFUL_RECORDS_FILE", "to_remediate.csv");
        String skipEntitiesFile = prop.getProperty("FAILED_RECORDS_FILE", "not_to_remediate.csv");
        String errorsEntitiesFile = prop.getProperty("ERRORS_ENTITIES_FILE", "errors.csv");

        System.out.format("USER=%s \n," +
                        " PASSWORD=%s \n" +
                        " SUCCESSFUL_RECORDS_FILE=%s \n" +
                        " FAILED_RECORDS_FILE=%s \n" +
                        " INPUT_FILE=%s \n" +
                        " PROCESSED_ENTITIES_FILE=%s \n" +
                        " AUTH_URL=%s \n" +
                        " TENANT=%s \n" +
                        " SERVER_URI=%s \n" +
                        " START ROW=%s \n" +
                        " TYPES TO PROCESS=%s \n" +
                        " ERRORS FILE=%s \n" +
                        " DO VERIFICATION=%s \n" +
                        " THREADS=%s \n" +
                        " DO REMEDIATION=%s \n" ,
        prop.getProperty("USER") ,
        prop.getProperty("PASSWORD") ,
                toRemediateFile,
                skipEntitiesFile,
        prop.getProperty("INPUT_FILE") ,
                processedEntitiesFile,
        prop.getProperty("AUTH_URL") ,
        prop.getProperty("TENANT") ,
        prop.getProperty("SERVER_URI"),
        prop.getProperty("START_ROW"),
        prop.getProperty("TYPES_TO_PROCESS"),
                errorsEntitiesFile,
        prop.getProperty("DO_VERIFICATION"),
        prop.getProperty("THREADS"),
        prop.getProperty("DO_REMEDIATION")
        );
        AuthClient authClient = new AuthClient(
                prop.getProperty("AUTH_URL"),
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"));

        RestApiClient restApiClient = new RestApiClient(
                prop.getProperty("SERVER_URI"),
                prop.getProperty("TENANT"),
                authClient
        );

        boolean doVerification = Boolean.parseBoolean(prop.getProperty("DO_VERIFICATION", "false"));
        boolean doRemediation = !doVerification && Boolean.parseBoolean(prop.getProperty("DO_REMEDIATION", "false"));
        if (doRemediation) {
            logger.info("Start remediation!!!");
            Thread.sleep(5 * 1000);
        }

        Collection<String> alreadyProcessedEntities = getProcessedEntities(processedEntitiesFile);

        InputDataProvider inputDataProvider = new InputDataProvider(prop.getProperty("INPUT_FILE"));
        Iterator<String> stringIterator = inputDataProvider.getStringIterator();

        AtomicLong successfulRequestCount = new AtomicLong(0);
        AtomicLong failedRequestCount = new AtomicLong();
        AtomicLong requestCount = new AtomicLong(0);
        Set<String> failedInputData = new ConcurrentSkipListSet<>();

        int threads = Integer.parseInt(prop.getProperty("THREADS", THREADS_DEFAULT));
        ExecutorService executor = Executors.newFixedThreadPool(threads);

        boolean appendToExistingFiles = alreadyProcessedEntities.size() > 0;

        if (appendToExistingFiles) {
            logger.info("There are already processed files!!!");
            logger.info("to_remediate and not_to_remediate files will be opened in append mode!!!");
            Thread.sleep(5 * 1000);
        }

        FileAppender successFileAppender = new FileAppender(toRemediateFile, appendToExistingFiles);
        FileAppender failureFileAppender = new FileAppender(skipEntitiesFile, appendToExistingFiles);
        FileAppender processedEntitiesLog = new FileAppender(processedEntitiesFile, true);
        FileAppender errorsFileAppender = new FileAppender(errorsEntitiesFile, appendToExistingFiles);

        // write headers to output files
        successFileAppender.accept(Collections.singletonList(RestApiClient.toRemediateRow("EntityId", "WinnerUri", "Attribute+Hub_Callback", "HasCountry", "HasReltioCrosswalk", "CountryCode")));
        failureFileAppender.accept(Collections.singletonList(RestApiClient.toExcludeRow("EntityId", "HasCountry", "HasReltioCrosswalk", "CountryCode","HasDCR", "Reason")));

        List<String> buffer = new ArrayList<>();

        AttributesRulesResolver.initRules(prop);
        EntitiesRulesResolver.initRules(prop);

        int rowIndex = 1;
        int startRow = prop.getProperty("START_ROW") != null ? Integer.parseInt(prop.getProperty("START_ROW")) : 1;
        while (stringIterator.hasNext()) {
            if (rowIndex++ < startRow) {
                continue;
            }
            String inputString = stringIterator.next();
            buffer.add(inputString);
            if (buffer.size() >= 50) {
                executor.submit(new BatchWorker(errorsFileAppender, processedEntitiesLog, alreadyProcessedEntities, restApiClient, buffer,
                        successfulRequestCount, failedRequestCount, requestCount, failedInputData, successFileAppender, failureFileAppender, doRemediation, doVerification));
                buffer.clear();
            }
        }
        if (!buffer.isEmpty()) {
            executor.submit(new BatchWorker(errorsFileAppender, processedEntitiesLog, alreadyProcessedEntities, restApiClient, buffer,
                    successfulRequestCount, failedRequestCount, requestCount, failedInputData, successFileAppender, failureFileAppender, doRemediation, doVerification));
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        successFileAppender.finalHook();
        failureFileAppender.finalHook();
        processedEntitiesLog.finalHook();
        errorsFileAppender.finalHook();

        if (!failedInputData.isEmpty()) {
            logger.info("----------------------------------------------------");
            logger.info("FailedInputData:");
            failedInputData.forEach(logger::info);
            logger.info("----------------------------------------------------");
        }
        logger.info(String.format(
                "Final results: requestCount=%s, successfullRequestCount=%s, failedRequestCount=%s",
                requestCount.get(), successfulRequestCount.get(), failedRequestCount.get())
        );
        logger.info("Finished.took total time=" + (System.currentTimeMillis() - START_TIME)/ (1000.0*60));
        logger.info("Finished");
    }

    private static Collection<String> getProcessedEntities(String processedEntitiesFile) {
        HashSet<String> processedEntities = new HashSet<>();
        InputDataProvider inputDataProvider = new InputDataProvider(processedEntitiesFile);
        Iterator<String> stringIterator = inputDataProvider.getStringIterator();
        while (stringIterator.hasNext()) {
            String row = stringIterator.next();
            String[] data = row.split(",");
            processedEntities.add(data[0]);
        }
        return processedEntities;
    }

}
