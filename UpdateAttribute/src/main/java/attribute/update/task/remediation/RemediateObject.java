package attribute.update.task.remediation;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RemediateObject {
    private String attributeUri;
    private String hubCallbackCrosswalkUri;
    public RemediateObject(String attributeUri, String hubCallbackCrosswalkUri) {
        this.attributeUri = attributeUri;
        this.hubCallbackCrosswalkUri = hubCallbackCrosswalkUri;
    }
    public static RemediateObject toRemediate(String attributeUri, Optional<JSONObject> entity) {
        if (entity.isPresent()) {
            // modified to get all HubCallback CW uris separated by ','
            String hubCallbackCrosswalkUri = extractHubCallbackCrosswalks(entity.get());
            return new RemediateObject(attributeUri, hubCallbackCrosswalkUri);
        } else {
            return new RemediateObject(attributeUri, null);
        }// we should have case where attributeUri is  null and have just crosswalkURI
    }

    private static String extractHubCallbackCrosswalks(String attributeUri, JSONObject entity) {
        JSONArray crosswalks =  entity.getJSONArray("crosswalks");
        String hubCallbackCrosswalkUri = null;
        for (int i = 0; i < crosswalks.length(); i++) {
            JSONObject crosswalk  = crosswalks.getJSONObject(i);
            String type = crosswalk.getString("type");
            if(type.equals("configuration/sources/HUB_Callback")){
                JSONArray attributes = crosswalk.getJSONArray("attributes");
                if (attributes != null) {
                    for (int j = 0; j < attributes.length(); j++) {
                        if (attributeUri.equals(attributes.getString(j))) {
                            hubCallbackCrosswalkUri = crosswalk.getString("uri");
                            break;
                        }
                    }
                    if (hubCallbackCrosswalkUri != null) {
                        break;
                    }
                }
            }
        }
        return hubCallbackCrosswalkUri;
    }

    private static String extractHubCallbackCrosswalks(JSONObject entity) {
        JSONArray crosswalks =  entity.getJSONArray("crosswalks");
        List<String> hubCallbackCrosswalkUris = new ArrayList<>();
        for (int i = 0; i < crosswalks.length(); i++) {
            JSONObject crosswalk  = crosswalks.getJSONObject(i);
            String type = crosswalk.getString("type");
            if(type.equals("configuration/sources/HUB_Callback")){
                hubCallbackCrosswalkUris.add(crosswalk.getString("uri"));
            }
        }
        return hubCallbackCrosswalkUris.isEmpty() ? null: String.join(",", hubCallbackCrosswalkUris);
    }

    public String getAttributeUri() {
        return attributeUri;
    }


    public void setAttributeUri(String uri) {
         this.attributeUri = uri;
    }

    public String getHubCallbackCrosswalkUri() {
        return hubCallbackCrosswalkUri;
    }

    public void clearHubCallbackCrosswalkUri() {
        hubCallbackCrosswalkUri = null;
    }

    public void setHubCallbackCrosswalkUri(String uri) {
        hubCallbackCrosswalkUri = uri;
    }
}
