package attribute.update.task.excluderules;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.Properties;

public class AttributesRulesResolver {

    public static void initRules(Properties properties) {
        ExcludeAttributeByTypeAndName.getInstance().init(properties.getProperty("EXCLUDE_ATTRIBUTES_BY_TYPE_AND_NAME"));
        ExcludeAttributeBySLN.getInstance().init(Arrays.asList(properties.getProperty("IGNORED_SLN_COUNTRIES").split(",")));
    }

    public static boolean attributeExcluded(String countryCode, String attributeName, JSONObject attribute, String entityType) {
        return ExcludeAttributeBySLN.getInstance().excluded(countryCode, attributeName)
                || ExcludeAttributeByTypeAndName.getInstance().excluded(entityType, attributeName);
    }
}
