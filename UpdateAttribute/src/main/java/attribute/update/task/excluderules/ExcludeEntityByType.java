package attribute.update.task.excluderules;

import java.util.Collections;
import java.util.List;

public class ExcludeEntityByType {
    private static final ExcludeEntityByType instance = new ExcludeEntityByType();
    private List<String> typesToProcess = Collections.emptyList();

    private ExcludeEntityByType() {
    }
    public static ExcludeEntityByType getInstance() {
        return instance;
    }
    public void init(List<String> typesToProcess) {
        if (typesToProcess != null) {
            this.typesToProcess = typesToProcess;
        }
    }
    public boolean entityExcluded(String entityType) {
        return !typesToProcess.isEmpty() && !typesToProcess.contains(entityType);
    }
}
