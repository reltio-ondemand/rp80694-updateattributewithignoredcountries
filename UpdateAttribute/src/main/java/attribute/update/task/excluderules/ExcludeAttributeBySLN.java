package attribute.update.task.excluderules;

import java.util.List;

public class ExcludeAttributeBySLN {
    private static ExcludeAttributeBySLN instance = new ExcludeAttributeBySLN();
    List<String> ignoredSLNCountries;
    private ExcludeAttributeBySLN() {
    }
    public static ExcludeAttributeBySLN getInstance() {
        return instance;
    }
    public void init(List<String> excludeSLNCountries) {
        ignoredSLNCountries = excludeSLNCountries;
    }
    public boolean excluded(String countryCode, String attributeName) {
        return !ignoredSLNCountries.isEmpty() && ignoredSLNCountries.contains(countryCode) && "License".equals(attributeName);
    }

}
