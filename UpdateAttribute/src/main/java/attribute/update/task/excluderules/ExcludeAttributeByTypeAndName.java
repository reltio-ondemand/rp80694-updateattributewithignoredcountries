package attribute.update.task.excluderules;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ExcludeAttributeByTypeAndName {
    private static ExcludeAttributeByTypeAndName instance = new ExcludeAttributeByTypeAndName();
    private Map<String, Collection<String>> rules = new HashMap<>();
    private ExcludeAttributeByTypeAndName() {
    }
    public static ExcludeAttributeByTypeAndName getInstance() {
        return instance;
    }
    public void init(String ruleStrProperty) {
        if (ruleStrProperty != null) {
            String[] perTypeRules = ruleStrProperty.split(";");
            for (String perTypeRule: perTypeRules) {
                String[] parts = perTypeRule.split(":");
                String type = parts[0];
                String[] attributeNames = parts[1].split(",");
                rules.put(type, Arrays.asList(attributeNames));
            }
        }
    }

    public boolean excluded(String entityType, String attributeName) {
        Collection<String> attributeNames = rules.get(entityType);
        return attributeNames != null && attributeNames.contains(attributeName);
    }
}
