package attribute.update.task.excluderules;

import attribute.update.task.remediation.RemediateObject;
import org.apache.log4j.LogManager;

import java.util.List;

public class ExcludeByDcrDetails {

    protected static final org.apache.log4j.Logger logger = LogManager.getLogger(ExcludeByDcrDetails.class);


    public static boolean isExcludeFromDCR(String status, String action, List<RemediateObject> objectList,
                                           String attributeDCR) {
        boolean result = false;
        if (status.equals("APPLIED") && action.equals("IGNORE_ATTRIBUTE")) {
            for (int i = 0; i < objectList.size(); i++) {
                if (objectList.get(i).getAttributeUri().contains(attributeDCR)) {
                    logger.info("DCR --> IGNORE_ATTRIBUTE->" + action + "-" + attributeDCR);
                    objectList.get(i).setAttributeUri("dummy");
                    result = true;
                }
                objectList.removeIf(e -> e.getAttributeUri().equals("dummy"));
            }
        }
        return result;
    }

    /*

    public static boolean isExcludeFromDCR1(String status, String action, List<RemediateObject> objectList,
                                           String attributeDCR) {
        int org = objectList.size();
        if (status.equals("I")) {
            logger.info("remediateObject-> " + objectList.stream().filter(e -> e.getAttributeUri()
                    .contains(attributeDCR.substring(attributeDCR.lastIndexOf("/"))))
                    .map(e -> e.getAttributeUri()).collect(Collectors.joining(",")) + "DCR-> " + attributeDCR);
            objectList.removeIf(e -> e.getAttributeUri().contains(attributeDCR.substring(attributeDCR.lastIndexOf("/"))) && e.getHubCallbackCrosswalkUri() == null);

            return org != objectList.size();
        }

        return false;
    }*/
}
