package attribute.update.task.excluderules;

import attribute.update.task.http.AuthClient;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class ExcludeEntityByDCR {
    private static ExcludeEntityByDCR instance = new ExcludeEntityByDCR();
    private AuthClient authClient;
    private String tenant;
    private String serverUri;

    private ExcludeEntityByDCR() {
    }
    public static ExcludeEntityByDCR getInstance() {
        return instance;
    }
    public void init(AuthClient authClient, String serverUri, String tenant) {
        this.authClient = authClient;
        this.tenant = tenant;
        this.serverUri = serverUri;
    }
    public boolean entityExcluded(String entityId) {
        return false;
     /*   try {
            String req = String.format("%s/api/%s/entities?filter=",
                    this.serverUri,
                    this.tenant);
            String filter = String.format("equals(type,'configuration/entityTypes/ChangeRequestLog') and equals(attributes.ovUri,'entities/%s')",
                    entityId);
            req += URLEncoder.encode(filter, "UTF-8");
            HttpGet findDCR = new HttpGet(req);
            findDCR.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
            findDCR.setHeader("Content-Type", "application/json");
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(findDCR);
            String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
            JSONArray dcrs  = new JSONArray(responseContent);
            //return dcrs.length() > 0;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;*/
    }
}
