package attribute.update.task.excluderules;

import attribute.update.task.http.AuthClient;

import java.util.Arrays;
import java.util.Properties;

public class EntitiesRulesResolver {
    public static void initRules(Properties properties) {
        AuthClient authClient = new AuthClient(
                properties.getProperty("AUTH_URL"),
                properties.getProperty("USER"),
                properties.getProperty("PASSWORD"));
        ExcludeEntityByDCR.getInstance().init(authClient, properties.getProperty("SERVER_URI"), properties.getProperty("TENANT"));
        String entitiesToProcess = properties.getProperty("TYPES_TO_PROCESS");
        if (entitiesToProcess != null) {
            ExcludeEntityByType.getInstance().init(Arrays.asList(entitiesToProcess.split(",")));
        }
    }
    public static boolean entityExcludedDueDCR(String entityId) {
        return ExcludeEntityByDCR.getInstance().entityExcluded(entityId);
    }
    public static boolean entityExcludedByType(String entityType) {
        return ExcludeEntityByType.getInstance().entityExcluded(entityType);
    }
}
