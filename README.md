# To collect attributes to unignore and crosswalks to be deleted
- Set DO_REMEDIATION=false
- Set INPUT_FILE=<path_to_entities_to_verify>
- Set PROCESSED_ENTITIES_FILE=<path_to_file_already_verified_entities>
- Set SUCCESSFUL_RECORDS_FILE=<path_to_results>
- Set FAILED_RECORDS_FILE=<path_to_file_skip_entities>
# To remediate attributes
- Set DO_REMEDIATION=true
- Set INPUT_FILE=<path_to_results_file>
- Set PROCESSED_ENTITIES_FILE=<path_to_file_already_remediated_entities>
- Set SUCCESSFUL_RECORDS_FILE=<path_to_remediated_entities>
- Set FAILED_RECORDS_FILE=<path_to_file_skip_entities>

# Steps
<ol>
<li>Execute script in DO_REMEDIATION=false providing ONLY not empty INPUT_FILE</li>
<li>Set INPUT_FILE = SUCCESSFUL_RECORDS_FILE from first step</li>
<li>Be sure INPUT_FILE not equals to SUCCESSFUL_RECORDS_FILE</li>
<li>Delete all files except INPUT_FILE which is point to to_remediate file</li>
<li>Execute script in DO_REMEDIATION=true</li>
</ol>
